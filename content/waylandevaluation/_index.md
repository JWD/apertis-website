+++
date = "2015-11-18"
weight = 100

title = "Wayland Evaluation"

aliases = [
    "/old-wiki/WaylandEvaluation"
]
+++

# Project Background

One of the bigger architectural decisions made during the early design
discussions (around 2011) was the selection of X Window System
(specifically the X.org implementation) as the display server instead of
the then up and coming Wayland project.

The main criteria used to select between Wayland and X.org at that time
were the following:

1.  Overall maturity of the selected solution (either current or
    expected during the development phase of the project).
2.  Available support for the selected solution in the open-source
    components (toolkits etc)
3.  Hardware support as provided for the selected solution (Intel and
    ARM targets)

Looking at the short term (less one or to two years), the following was
concluded at that point:

1.  X.org had been mature for a long time, Wayland was not expected to
    mature fast enough to support the intended feature development from
    early-on.
2.  Support for X in all components is mature, support for Wayland was
    either non-existent or early proof of concept.
3.  X HW enablement was committed to by the various HW providers,
    Wayland HW enablement not further discussed.

Given these answers, X was the clear winner at the time. Of course,
three years later (late 2014) a lot has changed, which means it's time
to re-assess these decisions.

1.  Wayland has evolved and matured a lot and has already been used in
    various commercial products.
2.  Wayland support in most of the components used now has the required
    level of maturity to be the basis of future developments
3.  SoC vendors are switching their focus to enabling Wayland for their
    products, spending either zero or substantially less time on X11
    enablement.

Given this re-evaluation, X is no longer clearly ahead of Wayland. As
such, a more detailed evaluation about the viability and benefits of
switching to a Wayland solution is being undertaken.

# X window system background

For a good understanding of the current situation of the X window system
(often referred to as X11 or X), a bit of history behind its design is
required and the constraints in which it was designed.

To do so, one has to go all the way back to the 1980s when X started,
back when graphics cards were no more than simple framebuffers and
memory and CPU resources were limited to levels unimaginably low by
today's standards. As a result, an extensible network protocol was
designed (such that applications could run on bigger backend machines).
This protocol allowed clients to send very basic drawing commands to the
server, which would draw directly to the screen output. Using this
strategy the memory requirements were kept to a minimum at the expense
of redrawing artifacts, which was perfectly acceptable in a time when
color displays were still a novelty and 256 colors the state of the art.
In addition, to position the various application windows on the screen,
without putting policy in the server, the concept of a window manager
was invented. The window manager ran as a separate application and took
responsibility for communicating to the X server where to place the
application windows and draw the window decoration (title bar etc) for
each window when required. The window manager however never had to touch
or to inspect the content of the application windows.

Since then, a lot has changed, not only has hardware become much more
capable but our expectations of a graphical user interface changed
dramatically. Due to the extensibility of the X11 protocol, the X server
has been able to keep evolving, without losing backwards compatibility.
Crucial for this evaluation are two main changes of how a modern X11
based desktop works:

  - Rendering moved from server side to the clients.
  - The responsibility of the final on-screen content moved from the X
    server to the compositor

As applications required more precise control over their graphical
output, the very basic drawing operations as provided by the X protocol
soon proved to be inadequate. At the same time, more memory became
available which made it viable for applications to have a (partial) copy
of their graphical output. Furthermore, the general trend was started to
move away from the thin-client based model for which the remote
rendering approach taken by X11 was designed. As such, over time,
applications moved from rendering by sending drawing commands to the X
server to doing all the drawing locally and only sending the resulting
image to the server for final display.

More recently desktop-wide animations started becoming the norm, of
which the Exposé feature in Mac OS X is a good example. The simple
window placement features provided by the X server however are far from
suitable for these animations. As such, the X protocol gained several
more extensions which added the ability for applications to draw their
windows on an off-screen area instead of directly on the screen. More
extensions were added to allow a so-called compositor (e.g. Mutter) to
combine all the windows in the offscreen area and render the final
onscreen output.

While the extensibility of X11 allowed it to still be relevant 25 years
after its original design, it has come at a cost. The requirement to be
able to still support all core features already present 25 years ago
means odd limitations shine through in various places. One of the main
user-visible examples of this is the inability to interact with windows
transformed by the compositor (e.g. zoomed, rotated) as X simply doesn't
have the ability to map the inputs to the transformed window. Looking at
the lower levels, the EXA 2D acceleration architecture in X.org isn't a
very good fit for the way modern graphics cards work, which means it
often causes a performance slowdown instead of a speedup unless
implemented with great care in the driver. In a lot of ways, X11 has,
over time, become a square peg that has with some success been fitted
into a round hole, but still doesn't really fit. The general consensus
around X in the open source community is that it has reached the end of
the line, it has been stretched as far as it could go and in some cases
even further.

# Introduction to Wayland

Wayland is a display server protocol targeted at modern graphics
environments, with a stated goal of "every frame is perfect". Wayland
itself is not a complete display system, instead it provides an
extensible protocol definition and C helper libraries to write Wayland
display servers and clients. The Wayland project also provides a
reference display server, called Weston (to which people often refer to
when talking about a fully functional Wayland system).

While Wayland has had a reasonably slow start (started as a spare-time
project back in 2008) development sped up considerably around 2012
[GNOME targeted Wayland](https://wiki.gnome.org/Initiatives/Wayland) as
an option for the 3.10 release (September 2013), which gave another push
at application and library support for Wayland. The recently released
Fedora 21 and the upcoming (as of late 2014) Debian Jessie release both
ship with the option of running GNOME (3.14) on Wayland. GNOME 3.16 and
Fedora 22 plan to ship with Wayland running the login
screen[1](http://blogs.gnome.org/mclasen/2015/01/19/gnome-wayland-porting-the-endgame/)[2](http://blogs.gnome.org/uraeus/2015/01/19/planning-for-fedora-workstation-22/)
(but not the default full session) on all devices, as a large-scale
deployment test. If that goes well, a complete switch to Wayland for
Fedora 23 is expected.

One thing to note about the deployments in general purpose distributions
such as Debian and Fedora, versus deployments in Apertis is that the bar
for usage as the default windowing system on a general purpose
distributions is much higher. Both with respect to the range of hardware
that needs to be support (different video hardware, input devices (mice,
tablets, touchpads etc), output device (mixed resolution monitors,
projectors etc) and with the range of software that needs to be support
(lots of different toolkits, legacy applications etc).

When compared to the X11 rendering model, there are two fundamental
changes:

  - The server exposes no rendering/drawing functionality to the
    applications at all.
  - The display server and compositor are one process and all hardware
    support is delivered by the compositor.

Enforcing applications to do all their rendering locally essentially
encodes current best practices and avoids the need for any complex and
expensive synchronization when mixing client and server rendering, which
often results in bad performance unless very finely tuned. Another
advantage is that this choice allows for a much cleaner separation of
responsibilities of the client and server; The client creates the
content, the server puts it on the screen.

The merger of the display server and the compositor effectively cuts out
the middle-man and puts the compositor in full control of the final
output and hardware. When using X11, the compositor implementation was
limited to using the APIs exposed by the X server, which essentially
meant the use of GL ES was the only practical choice for compositing.
For Wayland compositors, there is no such limitation: the Wayland
libraries and protocol make no assumptions about the methods used for
compositing in any way — this is left to the compositor implementation.
Especially for embedded hardware, this flexibility is very welcome as
these systems often have quite powerful display subsystems that were
essentially impossible to use effectively with X11. One extreme example
of using this flexibility is the [Raspberry Pi backend Collabora did for
Weston](http://fooishbar.org/tell-me-about/wayland-on-raspberry-pi/) in
2012–2014, which purely uses the Raspberry-specific DisplayManX APIs for
compositing resulting in much better performance than could be achieved
with EGL/GL ES based compositing on this hardware. Something which would
have been impossible to do with X.org in its current form.

Throughout this document, the Raspberry Pi is used as an example of the
potential of combining hardware specific APIs with conventional GLES.
The Pi was chosen as an example as it is an extreme: a low-power CPU
coupled with a very powerful display system, so it is a good
demonstration of the potential of hardware specific APIs for offloading
processing to the display subsystem and hence improving CPU performance.
Less extreme use-cases of this flexibility will typical mix GL ES based
compositing with usage of the display system, to get the best possible
overall system performance for each situation, an approach also used by
current Android systems.

A good example of the potential resource usage advantages offered by
Wayland over X11 was [demonstrated by Collabora at
SIGGRAPH 2014](https://www.collabora.com/about-us/blog/2014/08/13/wayland-x11-arm-mali/),
showing power usage of Wayland using stock EGL and KMS APIs on a
Chromebook 2 to be around 1/3 of that of X11 on the same hardware. While
power consumption is not a driving factor for automotive, it typically
directly corresponds to CPU and/or GPU resource usage. In this example,
cleverer usage of resources dramatically reduced power consumption
without user-visible changes. In other use-case the lower resource load
of the compositor can be used to allow applications to use more
resources (e.g. render a more complex GL scene).

# Wayland hardware enablement

As mentioned previously, the Wayland libraries and protocol do not
include any hardware integration or any strict requirements on the
hardware integration. All responsibility of dealing with the hardware
(both output and input) is offloaded to the compositor implementation
such as Weston (the Wayland project's reference compositor), Wayland
IVI/weston-ivi (formerly LayerManager; by Genivi) or Mutter (GNOME’s
compositor). Even though this seems to be a daunting task, a lot of the
actual work is done by existing support libraries and modern kernel
APIs, such as KMS, DRM, evdev and libinput. The number of hardware
interactions needed to be implemented for Wayland is also significantly
lower than the number needed to get X11 running on a particular device,
since Wayland operates exclusively on surfaces rather than in primitive
drawing operations. In return for this added complexity, a lot of
flexibility is gained by putting the compositor fully in the driver's
seat and not being limited to the API and protocol exposed by the
server. This allows the compositor to take full advantage of the
available hardware in ways that simply weren't possible with X11, which
includes using device-specific APIs and dynamically using overlays and
other hardware accelerated elements if possible without the need to
expose this to clients.

Several Wayland projects have based their compositor off Weston,
including Wayland IVI and the Raspberry Pi shell (Maynard). Doing so
gives a stable and mature platform to start from, using a code base
which was developed by the core Wayland developers (so uses the Wayland
protocol to its full advantage), and means the platform will receive
further updates and maintenance as Wayland development progresses,
reducing maintenance burden.

Wayland IVI is a GENIVI project to build an IVI platform based on
Wayland and Weston. It defines a set of protocol extensions to the core
Wayland protocols, and provides a reference compositor implementation,
weston-ivi, comprising the stock Weston compositor which includes an
IVI-specific shell (‘ivi-shell’), and a separate window manager process.

Both Weston and Wayland IVI compositors are built to be dynamic in the
way they work with the underlying hardware. For example Weston can
currently use DRM/KMS in combination with GLES and DRM-based overlays,
simple dumb framebuffers with pure software rendering, X11 with GLES
rendering or Raspberry Pi specific APIs to do the composition. Wayland
IVI as published by Genivi has similar capabilities, adding
Vivante/Freescale support into Weston with [a small series of
patches](https://github.com/Freescale/meta-fsl-arm/tree/master/recipes-graphics/wayland/weston).
A similar dynamic approach to handling input devices exists, but as
these interfaces are much more standardized (e.g. either libinput, or
evdev or X input when running on top of X11) thus not of high risk nor
interest for hardware enablement.

For a compositor to do useful things, it needs some content to composite
onto the screen. This brings us to the next step for hardware
enablement: transferring the pixel content from the application to the
compositor. In the Wayland protocol this revolves around two main
concepts: Surfaces, which are rectangular areas displayed on the screen;
and Buffers, which carry the actual pixel content to be displayed in
such Surfaces. To do screen updates, the application sends buffers with
the updated content to the compositor and asks the compositor to use it
to update its Surface. As with X11, the real challenge for performance
is to optimize the transfer between the application and the display
server/compositor. In this case, Wayland doesn't enforce how this
transfer happens, all that is required is that the application sends
buffers in a way the compositor can use and understand. Wayland itself
only defines a shared memory transport as a lowest common denominator to
be supported by all compositors. Other transports can be defined by the
platform libraries, for example the Mesa EGL/GLES library defines a DRM
transport, the details of which are completely hidden inside the GL
library itself and well away from both the compositor implementation and
the applications using Mesa.

With X11, three transfers need to occur before pixels can hit the screen
(client → X server → compositor → X server for display), while in
Wayland, the pixel data only has to be transferred between processes
once (between the compositor and the client). As such, even with the
same level of support for a particular hardware platform from Wayland
and X11, Wayland should in theory perform better than X11.

As a slight side-note to general hardware enablement, since version 1.0
the Wayland protocol has been stabilized. This means that any Wayland
hardware enablement done for at least Wayland 1.0 will not be broken or
require updates for moving to newer versions of Wayland, nor will
clients targetted at Wayland 1.0 break when moving to newer versions. A
similar stability policy exists for extension protocols, several of
which are in development for various features (such as xdg_shell) as of
the end of 2014. If any Apertis work is done against these protocols, it
will be future-proof once (and only once) the protocols are marked as
stable.

# Evaluation of Wayland for Apertis

The evaluation of Wayland for Apertis can generally be split up in three
parts:

  - Wayland support for the various target platforms
  - Wayland support from an application perspective
  - Impact of moving to Wayland on delivered and upcoming features

## Wayland support for various target platforms

Apertis currently targets three different hardware platforms, each with
slightly different requirements:

  - Freescale i.MX6 (Specifically the Sabrelite board)
    The compositor should directly drive the hardware and integrate with
    clients using the Vivante GL(ES) stack.
  - Intel native (Specifically the Minnowboard Max)
    The compositor should directly drive the hardware and integrate with
    the Mesa GL(ES) stack.
  - Intel SDK
    XFCE desktop system running in a VirtualBox image with software GL
    rendering. The compositor must run as a window in the standard X11
    environment using Mesa software rendering for GL support.

This leads to a requirement for a compositor with modular support for
graphics and input.. These considerations are further expanded in the
[Evaluation of compositors and
shells](#evaluation-of-compositors-and-shells) section.

For both the usage of Weston and Mutter as the baseline compositor, we
can take a closer look at the three platforms to support. Starting with
the easiest target, *Intel native*, as the main development target for
Weston and Mutter, this simply works out of the box without the need for
any further development. In a similar fashion, running Weston or Mutter
(as a Wayland compositor) under X11 as required for the *Intel SDK*
target is a very common use-case. However, one caveat was discovered
during the testing done for this evaluation: the current (late 2014)
Mesa support for Wayland does not work when using software-rendered GL
as Mesa doesn't currently support the default Wayland shared memory
transport but only the DRM transport which can't be used under software
rendering. Resolving this is a relatively small effort (in the order of
days), but something which will need to be fixed for Apertis
nonetheless.

The last target, and as always the hardest, is the Freescale i.MX6 based
ARM platform. The Yocto meta from Freescale contains several patches to
Weston to add support for the FSL graphics stack based on GL ES and the
frame buffer backend. Collabora has not tested the quality of that
backend at this point (early 2015). For Mutter either similar changes to
its native backend have to be done or a new backend would have to be
written to support GL (and the Vivante specifics) on the frame buffer.
However for better integration with the overall system (e.g. seamless
switching between boot splash and the graphical session), GL ES on top
of the standard DRM infrastructure would be preferable, but the level of
support from the FSL/Vivante graphics stack for that is unclear.

## Wayland for applications

For any display server system to be successful, it needs to provide the
features required by the applications using it. It was indicated that
the Apertis platform has no specific requirements for the use of X11, as
such, the evaluation for applications can purely focus on the features
and support libraries required by applications.

Crucially, this means that Apertis has a much more relaxed set of
requirements for Wayland to be production-ready: there is no need to
support legacy X11 applications, or a wide variety of graphics hardware.
This means that switching to Wayland on Apertis is a much smaller
problem than switching a full desktop environment such as GNOME. With
Wayland adoption at its present state in desktop Linux distributions,
the big steps have already been taken, and waiting for further
stabilisation on the desktop gives diminishing benefits for Apertis due
to its different trade-offs.

### Graphics, UI and toolkits

As the very first item, applications need to be able to put their UI on
the screen. For Apertis, most applications directly or indirectly use GL
ES for their rendering, as such, the first thing that needs to be in
place is support from the underlying EGL/GLES stack for Wayland.
Assuming that support is in place, what is left for the application is
to set up the connection to the window system, handle everything apart
from the rendering itself in a window system specific way (e.g. input).
For essentially all applications shipped by Apertis, this is handled by
the graphics libraries. The main graphics libraries used are MX, Clutter
and Cogl. Clutter and Cogl have had Wayland support since 2010, which
has been in active development and is quite mature. For MX, Wayland
support was added in MX 2 which is the only version currently supported
in Apertis. During the evaluation for this report test packages of Cogl,
Clutter and MX 2 were built with Wayland support and used to
successfully run an otherwise unmodified WebKit-Clutter on Wayland
(using Weston as the compositor) on the Intel platform.

### Input support

For Apertis three types of input are expected to be used:

  - Multitouch via the touchscreen
  - Onscreen keyboard
  - Emulated hardware buttons (out of screen buttons).

Because Wayland started in an era where touchscreens were common-place,
support for these is a first-class citizen. The touch model from Wayland
is comparable with that of X11, which makes supporting it in
applications which already support touch under X easy. Clutter includes
touch support under X and Wayland in the same way, so no changes are
required for touch-capable Clutter based application to support
multitouch under Wayland.

For onscreen keyboards, Collabora has recommended the use of the IBus in
the Internationalization proposal. A lot of effort has been going into
porting IBus to Wayland with the Wayland protocol experimentation taking
place by using the Weston compositor which is rapidly approaching a
mature state. For client applications, this means that by using the
GtkIMContext on top of IBus approach as outlined in the
Internationalization proposal will work for both Wayland and X11 without
further changes. The onscreen keyboard application itself of course also
needs to support Wayland and should integrate with the Wayland
compositor to provide a good user experience, similar to what needs to
happen on X11.

Emulated hardware buttons are a feature delivered for Apertis by
Collabora. When doing the same feature under Wayland, the compositor
implementation can take advantage of being in full control of the input
flow. Instead of providing an external daemon to recognize the hardware
buttons and emulate button presses via the uinput subsystem, this should
be done directly in the compositor. No changes to applications are
required. As such this feature will require an update for use with
Wayland.

In summary, Wayland is fully capable of fulfilling the input
requirements of the Apertis system.

# Evaluation of compositors and shells

On the assumption that Collabora’s recommendation of switching to
Wayland is accepted, we can explore the possibilities for the choice of
Wayland compositor and shell process to use. There are three main
options, and their pros and cons are explored in the following sections:

1.  Continue using Mutter, using its Wayland support
2.  Switch to using Weston plus a custom shell
3.  Switch to using Wayland IVI

Firstly, Mutter has gained support for Wayland from 2013 onwards, with
that support maturing as part of the GNOME project’s roadmap for moving
to Wayland. However, due to Mutter’s legacy as an X11 window manager (it
was originally a development of Metacity, which was an X11-only window
manager from GNOME 2), its architecture does not fit in perfectly with
the Wayland architecture and, as a result, further development in
response to new Wayland capabilities may be tricky without large
refactoring effort. Architecturally, Weston is (in its position as a
reference compositor) much more aligned with Wayland, and will remain
so. Wayland IVI has some serious architectural problems, notably its use
of an external process for window management with a very limited [layout
API](http://cgit.freedesktop.org/wayland/weston/tree/ivi-shell/ivi-layout-export.h),
which is a regression back to the X11 approach to window management. By
separating this between processes, it reduces reliability (two processes
to fail instead of one; the potential for synchronisation problems
between the processes; performance overhead and programming complexity
of IPC communications) as described in the sections above.

Both Mutter and Weston support the [xdg-shell Wayland
protocol](http://cgit.freedesktop.org/wayland/weston/tree/protocol/xdg-shell.xml)
for implementing management of client windows (such as minimisation,
fullscreening and maximisation). Wayland IVI introduces its own shell
protocol instead:
[ivi-application](http://cgit.freedesktop.org/wayland/weston/tree/protocol/ivi-application.xml),
which is smaller and less mature. Supporting xdg-shell is valuable, as
it is supported by all the major toolkits (GTK+, Clutter) as the
preferred mechanism for window management. In the case of GTK+, it is
required. It would be possible to modify GTK+ to use a different shell
protocol, but these modifications would likely not be accepted upstream.
On the other hand, there are a few parts of the xdg-shell protocol which
are not needed for touchscreen IVI systems, such as xdg_popup; these
could not be eliminated from the protocol, and stub implementations
would have to be present in the Apertis compositor.

Hardware support varies between the three. If using Mutter as the
compositor, the hardware interaction is constrained to using GL (ES), as
Mutter is based on Clutter, and Clutter is tied to GL (ES). This means
that existing effects can continue to be implemented using Clutter APIs,
but unfortunately means that non-GL APIs cannot be used for animations.
As the use of GL is deeply embedded in the design of Clutter, this would
not be very hard to change, so the use of display system APIs, would be
impossible. If Weston were used, such APIs could be opportunistically
used once support for them is added to Weston, which would be a
straightforward (though not small) task. If functionality not provided
by the device specific APIs is needed, Weston ‘falls back’ to using full
GLES.

Secondarily, if using GL for rendering of both applications and the
final compositing, each output pixel will run through the full GL GPU
pipeline twice: once when uploaded by the application, and a second time
when composited to produce the final frame. This may not happen if using
device-specific APIs, where pixels from applications will run through
the GPU once when uploaded by the application (e.g. if it’s using GLES
for in-window 3D effects), and then (depending on the device) may bypass
the GL pipeline and enter a simpler, more efficient, 2D framebuffer
engine to composite the overall frame instead. Especially for devices
with less capable GPUs and higher resolution screens (720p/1080p), this
second pass through GL GPU pipeline tends to be consume significant
amounts of GPU resources.

Vendor support is a highly related question. As vendors look to support
Weston, they will either choose to support normal Weston, or to support
Genivi’s Wayland IVI in their BSPs. It is unlikely that they would ship
support for Mutter, so any vendor-specific changes would have to be
ported from Weston to Mutter were option \#1 to be chosen. Whether
vendors support Weston or Wayland IVI remains to be seen. As of late
2014, Wayland IVI is quite young — the ivi-shell has only just been
merged upstream, and [Wayland IVI’s ivi-layout
API](http://cgit.freedesktop.org/wayland/weston/tree/ivi-shell/ivi-layout-export.h)
is still missing a number of large features (multiple IVI Screens; IVI
Surface on multiple IVI Layers; any kind of clipping). Some SoCs have
shipped with support for normal Weston so far, and it seems probable
that this will continue. Wayland IVI has a promise of testing,
maintenance and future development from Nobuhiko Tanibata at ADIT/Denso.

case, Clutter can continue to be used for all UI furniture (e.g. status
bar, sidebars etc) — it is only the shell/window manager animations
themselves which may need to change.

For the transition period from X11 to a Wayland based system, all
options make it possible to start testing application with the reference
compositor implementation (like Mutter was used before on X11), although
some initial work is required in all cases to enable the Freescale
platform. The next steps are slightly different for Mutter, as updating
the current Mutter plugin to a more recent Mutter version and removing
the X11 specific code would allow running both the X11 and Wayland
sessions with the same codebase, easing side-by-side comparisons. In the
other cases, after porting the Mutter plugin code to e.g. Weston, the
X11 session would still require Mutter to run.

The downside of switching from Mutter is that a single scene graph
toolkit could no longer be used for all animations: windowing animations
would be implemented using the Weston animation framework, while
in-application animations would continue using Clutter. This would make
writing windowing animations slightly harder (having to learn a second
API), but given the number of windowing animations which will be written
tends to be limited small. We believe this disadvantage is outweighed by
the technical benefits of switching to Weston in the longer term.

## Conclusion

From a purely technical perspective our recommendation is **option
\#2**: switching to using Weston as the compositor and window manager,
in combination with a custom shell. In several pieces of extra work
would need to be done:

  - Freescale platform enabled
  - Application testing under Wayland (and porting where needed)
  - New shell developed
  - Existing Mutter plugin would have to be retired and integrated with
    the new shell
      - Splash screen and fixed background image moved across
      - Gesture support moved across and reimplemented using libinput
      - Hardkey support moved across and reimplemented in the compositor
        process, eliminating the separate daemon and allowing many
        simplifications
      - Window management animations ported from Clutter to Weston’s
        animation framework

This approach is very similar to that used for Raspberry Pi: Weston as
the compositor, plus a custom shell called
[Maynard](http://blog.barisione.org/2014-04/maynard/) to implement the
desktop chrome. (This is technically a ‘shell helper client’ for Weston,
replacing `weston-desktop-shell`.)

However from a transition and project point of view, especially given
that Mutter’s current performance is adequate on X11, it might be
recommendable to go for **option \#1**: use Mutter as a Wayland
compositor as a first step of transition to Wayland. Top level tasks
that are required for this are:

  - Application testing under Wayland (and porting where needed)
  - Existing Mutter plugin would have to updated for a new mutter
    version and X11 specific code removed.
  - Hardkey support implemented in the compositor process, eliminating
    the separate daemon and allowing many simplifications
  - Virtualisation support when running the infotainment domain in a
    virtualized environment
  - Ensure proper functionality of the compositor when used in an X11
    environment as present on the the SDK
  - Enable face-plate functionality when running in the SDK environment
    (currently provided by Xoo)

# Virtualisation

The existing virtualisation system which separates the automotive and
infotainment systems would have to be adapted if Wayland were to be
used.

A virtualisation system which prevents clients accessing hardware
devices is possible to implement for Wayland by passing all GL commands
through a trusted computing base (TCB): either the compositor, or an
intermediary. This would allow validation of GPU command streams from
each client, with the potential of preventing malicious commands being
sent to the GPU.

Instead of a client sending GL commands directly to the GPU device node
via Clutter, a GL wrapper library is loaded instead, which serialises
all GL commands from the client and sends them via a transport (such as
shared memory or an Ethernet connection) to the trusted computing base
(in our case the automotive domai). The TCB can then deserialise the
commands, optionally validate them and execute them on the GPU in the GL
context i has created for that client. This ensures that all GL contexts
are kept within the TCB, and hence only the TCB needs access to the GPU
device. The TCB would return any return values (such as opaque buffer
handles) to the client.

This setup works for both Wayland and X11 clients in a very similar
fashion. Where in the X11 case the buffer handles are are transported to
the X server using DRI2 for a Wayland based compositor a custom protocol
(similar to the one used by mesa) would be provided to share the buffer
handles.

For virtualizing the compositor, the situation is currently quite a bit
more complex as the infotainment domain is X11 based and the automotive
domain is Wayland based. This means that in the infotainment domain, the
tasks of the display server are split between the X11 server and Mutter
and have to be consolidated in the automotive domain to have one
consistent virtual display server (which is a nested compositor in the
current implementation). In case the infotainment domain also switches
to Wayland, a lot of the complexity goes away as the Wayland compositor
becomes the sole process responsible for the display server tasks.

To conclude, while switching to Wayland does require some core changes
in the virtualisation implementation, the fundamental concepts can stay
the same and in some case can even be simplified.

# Afterword

From 2012 or so onwards Wayland and Wayland support has come a long way
in the open source community, making a switch to Wayland a viable option
for the Apertis middleware. Event though some effort will be required to
make the switch, in return Apertis will contain a more modern,
future-proof and flexible display system. That alone makes a switch to
Wayland well worth considering, given the Apertis baseline is meant to
be build on further for future products in the coming years.

Looking outside of the Apertis middleware and specifically to the ARM
hardware enablement challenges we've seen and will keep seeing in the
future with Freescale and Vivante, Wayland seems a promising candidate
to help face those challenges, not only because some of the technical
advantages as mentioned in the report, but probably more so because of
the interest and push for Wayland we're currently seeing. More generally
from Genivi, which makes Wayland support very interesting for vendors
targeting the IVI market or even the general Linux platform. X11 on the
other hand isn't seeing much interest from the embedded world, which
means from an SOC vendors point of view it's simply not very attractive
to invest in. As a result, the level of X11 support provided by SOC
vendors is expected to decline over time in favour of supporting
Wayland.

# References

  - [Wayland IVI](https://at.projects.genivi.org/wiki/display/PROJ/Wayland+IVI+Extension)
  - [Wayland IVI extension repository](https://github.com/GENIVI/wayland-ivi-extension)
  - [Maynard introduction](http://blog.barisione.org/2014-04/maynard/)
  - [Maynard home page](https://github.com/raspberrypi/maynard/wiki)
