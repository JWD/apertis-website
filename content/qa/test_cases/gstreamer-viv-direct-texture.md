+++
date = "2016-10-30"
weight = 100

title = "gstreamer-viv-direct-texture"

aliases = [
    "/old-wiki/QA/Test_Cases/gstreamer-viv-direct-texture"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
