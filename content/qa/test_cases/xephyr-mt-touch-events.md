+++
date = "2017-10-25"
weight = 100

title = "xephyr-mt-touch-events"

aliases = [
    "/old-wiki/QA/Test_Cases/xephyr-mt-touch-events"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
