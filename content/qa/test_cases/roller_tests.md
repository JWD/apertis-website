+++
date = "2017-12-01"
weight = 100

title = "roller_tests"

aliases = [
    "/qa/test_cases/roler_tests.md",
    "/old-wiki/QA/Test_Cases/Roler_tests",
    "/old-wiki/QA/Test_Cases/Roller_tests"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
