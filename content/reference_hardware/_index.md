+++
date = "2018-05-23"
weight = 100

title = "Reference Hardware"

aliases = [
    "/old-wiki/Deployment_Environment",
    "/old-wiki/Reference_Hardware"
]
+++

Apertis currently supports a selection of reference hardware designs. These
span the Intel 64-bit, ARM 64-bit and ARM 32-bit architectures:

- [Intel 64-bit reference hardware]( {{< ref "/reference_hardware/amd64.md" >}} )
- [ARM 32-bit reference hardware]( {{< ref "/reference_hardware/arm32.md" >}} )
- [ARM 64-bit reference hardware]( {{< ref "/reference_hardware/arm64.md" >}} )
- [Optional extra reference hardware]( {{< ref "/reference_hardware/extras.md" >}} )

Various prebuilt [images]( {{< ref "images.md" >}} ) are available for testing.
Apertis development is fasted paced, so we recommend that you use the daily
build images whenever possible.

If you currently don't have access to any of the supported hardware, the
`amd64` images can be run on a
[virtual machine]( {{< ref "/guides/virtualbox.md" >}} ).

